package com.company;

public class CalcularMontante {

    double taxaRendimento = 0.70;
    double montanteAcumulado = 0.00;
    //SolicitacaoCliente solicitacaoCliente;


    //public double getCalculoMontante(double capital, int qtd){
    //No comentario acima eu passava 2 parametros para que o GetCalculoMontante fizesse o que tem que fazer,
    //Aí abaixo eu substitui o de cima para mandar por apenas 1 objeto que contém as 2 variáveis
    public double getCalculoMontante(SolicitacaoCliente solicitacaoCliente){

        montanteAcumulado = solicitacaoCliente.getCapitalInvestido();

        for(int i = 0; i < solicitacaoCliente.getQuantidadeMeses(); i++){
            montanteAcumulado += montanteAcumulado * taxaRendimento;
        }

        return montanteAcumulado;
    }

}
