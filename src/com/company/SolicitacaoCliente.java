package com.company;

import java.util.Scanner;

public class SolicitacaoCliente {
    private double capitalInvestido = 0.00;
    private int    quantidadeMeses  = 0;

    public SolicitacaoCliente() {

        Scanner entradaCliente = new Scanner(System.in);
        System.out.printf("Digite o capital investido: ");
        capitalInvestido = entradaCliente.nextDouble();

        System.out.printf("Digite a quantidade de meses para o investimento: ");
        quantidadeMeses = entradaCliente.nextInt();

        return;
    }

    public double getCapitalInvestido() {
        return capitalInvestido;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }
}
