package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//Essa primeira parte é de forma estruturada pra aprender
        double capitalInvestido = 0.00;
        int    quantidadeMeses  = 0;
        double taxaRendimento = 0.70;
        double montanteAcumulado = 0.00;

        Scanner entradaCliente = new Scanner(System.in);
        System.out.printf("Digite o capital investido: ");
        capitalInvestido = entradaCliente.nextDouble();

        System.out.printf("Digite a quantidade de meses para o investimento: ");
        quantidadeMeses = entradaCliente.nextInt();

        montanteAcumulado = capitalInvestido;
        for(int i = 0; i < quantidadeMeses; i++){
            montanteAcumulado += montanteAcumulado * taxaRendimento;
        }

        System.out.println("O saldo total ao final do prazo será : R$ " + montanteAcumulado);
//Aqui pra cima é de forma estruturada......

//Essa segunda parte é pensando em orientação à objetos.
        SolicitacaoCliente solicitacaoCliente = new SolicitacaoCliente();

        CalcularMontante calculoMontante = new CalcularMontante();

        ExibeMontante exibeMontante =
        //new ExibeMontante(calculoMontante.getCalculoMontante(solicitacaoCliente.getCapitalInvestido(), solicitacaoCliente.getQuantidadeMeses()));
        //No comentario acima eu tirei o passagem de 2 variaveis e abaixo coloquei apenas 1 objeto (que contem as 2 variaveis de cima).
         new ExibeMontante(calculoMontante.getCalculoMontante(solicitacaoCliente));

    }
}
